Firts tests for STM32F7 Discovery board using AC6 System Workbench toolchain (Windows 8.1 x64)

This project was generated using ST CubeMX code generator.
As for the rest of the projects generated using CubeMX I have follow the steps described in the documentation
"Importing a STCubeMX generated project", at this link: [http://www.openstm32.org/Importing+a+STCubeMX+generated+project?structure=Documentation]

The build was without errors and the elf file was generated.

CubeMX project is this one: stm32f7_disco_first.ioc
Should be open with CubeMX without problems. I don't know if CubeMX will update the path for the new pc. So to test only the build step is better to not regenerate the project,
anyway could be open and inspected.

HOW TO RE-BUILD:
For build step should be follow the process described in documentation "Importing a STCubeMX generated project":

 - Open System Workbench for STM32. When SW ask for workspace browse to directory __stm32f7_disco_first_wa__. This is step 3 in the documentation (see link above)

 - Import the project as following
     File >> Import...
     Click on "Browse" and navigate to  
     "your_location"\stm32f7_disco_first_wa\stm32f7_disco_first\SW4STM32

     The projects list is refreshed and should display your project ("stm32f7_disco_first 
     Configuration"), select it.
     Ensure the option "Copy projects into workspace" is uncheck
     Click on "Finish" button.

 - Build the project

 - Check if the debugger configuration is present. If it is present try to debug. If not create
   a new one, following the steps described at above link. 

I hope this will work, I did't have time to test it.

Related to OpenOCD I have see that in last days (August 20, 2015) some commits were added for STM32 F7: http://openocd.zylin.com/#/c/2940/3

Regarding the CubeMX project, I selected Discovery board for STM32F7 and I didn't change any configuration, so it is the default configuration that came from ST. There are few peripherals that are enabled, but not all. Also seems that most of the Cortex-M7 features are disabled: ART Accelerator, CPU I,D cache, Instruction Prefetch - I let them as it was.

For other peripherals the ST provides examples in the Cube Library. For this board (STM32746G-Discovery):
 - 15 examples are provided to demonstrate simple usage of the existing peripherals (ADC, BSP, CEC, DAC, DMA, DMA2D, FLASH, FMC, I2C, LTDC, PWR, QSPI, RCC, SPI, TIM, UART).
 - 11 projects to demonstrate simple applications (Audio, Display, EEPROM, FatFs, FreeRTOS, LibJPEG, LwIP, QSPI, STemWin, USB_Device, USB_Host)
 - one big demo application- this is the application that came installed on the board.

All this examples and more or less documentation are inside the downloaded STM32Cube_FW_F7_V1.1.0 package. An easy way to locate where this folder is on pc is to go in CubeMx > Help > Updater Settings. Then see what is in input text box named "Repository Folder". Please note that System Workbench could also download this package but in another place.
